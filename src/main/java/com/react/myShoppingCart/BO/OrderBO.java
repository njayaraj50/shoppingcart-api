package com.react.myShoppingCart.BO;

import java.util.List;

public class OrderBO {

	private int userid,cartTotalQuantity;
	private double totalAmount;
	
	private List<OrderItemBO>cartItems;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getCartTotalQuantity() {
		return cartTotalQuantity;
	}

	public void setCartTotalQuantity(int cartTotalQuantity) {
		this.cartTotalQuantity = cartTotalQuantity;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<OrderItemBO> getCartItems() {
		return cartItems;
	}

	public void setCartItem(List<OrderItemBO> cartItems) {
		this.cartItems = cartItems;
	}

	@Override
	public String toString() {
		return "OrderBO [userid=" + userid + ", cartTotalQuantity=" + cartTotalQuantity + ", totalAmount=" + totalAmount
				+ ", cartItem=" + cartItems + "]";
	}

	
}
