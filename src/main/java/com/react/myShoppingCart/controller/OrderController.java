package com.react.myShoppingCart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.react.myShoppingCart.BO.OrderBO;
import com.react.myShoppingCart.entity.Order;
import com.react.myShoppingCart.entity.OrderItems;
import com.react.myShoppingCart.services.OrderService;

@RestController
@RequestMapping("/api/order")
@CrossOrigin(origins = "*")
public class OrderController {
	
    @Autowired
	private OrderService orderService;
   
	@PostMapping("/myorder")
	public Order saveOrder(@RequestBody OrderBO orderBo) {
		return orderService.placeOrder(orderBo);
		
	}
	
	@GetMapping("/vieworder")
	@ResponseBody
	public List<Order> viewOrderById(@RequestParam(value="userid")int userid) {
		return orderService.viewOrderDetails(userid);
	}
	
	@GetMapping("/vieworderdetails")
	@ResponseBody
	public List<OrderItems> viewOrderDetailsById(@RequestParam(value="orderid")int orderid) {
		return orderService.viewOrderItemDetails(orderid);
	}
}
