package com.react.myShoppingCart.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.react.myShoppingCart.entity.Product;
import com.react.myShoppingCart.services.ProductService;

@RestController
@RequestMapping("/api/product")
@CrossOrigin(origins = "*")
public class ProductController {
    
	@Autowired
	private ProductService productService;
	
	@GetMapping("/all")
	@ResponseBody
	public List<Product> viewAllProducts(){
		return productService.viewAllProducts();
	}
	
	@GetMapping("/productdetails/{id}")
	@ResponseBody
	public Product getProductById(@PathVariable(value="id")int id){
		return productService.viewProductById(id);
	}
}
