package com.react.myShoppingCart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.react.myShoppingCart.BO.UserBO;
import com.react.myShoppingCart.entity.User;
import com.react.myShoppingCart.services.UserService;



@RestController
@RequestMapping("/api/login")
@CrossOrigin(origins = "*")
public class UserController {
	
	@Autowired
	private UserService userService;
	@PostMapping("/register")
	public User addUser(@RequestBody UserBO userBo) {
		return userService.addNewUser(userBo);
	}
	@GetMapping("/checklogin")
	@ResponseBody
	public  User userLogin(@RequestParam(value="username")String username,@RequestParam(value="password")String password) {

		return userService.CheckLogin(username,password);
	}	
	@GetMapping("/profile")
	@ResponseBody
	public  User userProfile(@RequestParam(value="userid")int id) {

		return userService.getUserById(id);
	}
}
