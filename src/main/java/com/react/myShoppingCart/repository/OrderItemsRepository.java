package com.react.myShoppingCart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.react.myShoppingCart.entity.Order;
import com.react.myShoppingCart.entity.OrderItems;

@Repository
public interface OrderItemsRepository extends JpaRepository<OrderItems, Integer>{
	public List<OrderItems> findByOrder(Order order);
}
