package com.react.myShoppingCart.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.react.myShoppingCart.entity.Order;
import com.react.myShoppingCart.entity.OrderItems;
import com.react.myShoppingCart.entity.User;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>{
	
	
	public List<Order> findByUser(User user);

	
}
