package com.react.myShoppingCart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.react.myShoppingCart.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

}
