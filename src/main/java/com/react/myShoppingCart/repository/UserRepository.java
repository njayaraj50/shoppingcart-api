package com.react.myShoppingCart.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.react.myShoppingCart.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query("select user from User user where user.username=:username and user.password=:password")
	public Optional <User> findbyunamepassword(@Param(value="username")String username,@Param(value="password")String password);
}

