package com.react.myShoppingCart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.react.myShoppingCart.entity.Order;
import com.react.myShoppingCart.entity.OrderItems;
import com.react.myShoppingCart.repository.OrderItemsRepository;

@Service
public class OrderItemsService {
	
	@Autowired
	private OrderItemsRepository orderItemsRepo;
	
	public void saveAllOrderItems(List<OrderItems>orderItems) {
		orderItemsRepo.saveAll(orderItems);
	}

	
}
