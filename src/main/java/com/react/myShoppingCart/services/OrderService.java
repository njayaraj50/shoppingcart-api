package com.react.myShoppingCart.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.react.myShoppingCart.BO.OrderBO;
import com.react.myShoppingCart.BO.OrderItemBO;
import com.react.myShoppingCart.entity.Order;
import com.react.myShoppingCart.entity.OrderItems;
import com.react.myShoppingCart.entity.Product;
import com.react.myShoppingCart.entity.User;
import com.react.myShoppingCart.repository.OrderItemsRepository;
import com.react.myShoppingCart.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepo;
	@Autowired
	private OrderItemsRepository orderItemsRepo;
	@Autowired
	 private UserService userService;
	@Autowired
	private ProductService productService;
	@Autowired
	private OrderItemsService orderItemsService;
	
	public Order placeOrder(OrderBO orderBo) {
		User user=userService.getUserById(orderBo.getUserid());
		Order order= new Order();
		order.setQuantity(orderBo.getCartTotalQuantity());
		order.setTotalAmount(orderBo.getTotalAmount());
		order.setUser(user);
		orderRepo.save(order);
		if(order.getId()>0) {
			List<OrderItems>orderItems=new ArrayList<OrderItems>();
			List<OrderItemBO>orderItemBo=orderBo.getCartItems();
			for(OrderItemBO orderItem:orderItemBo) {
				Product product=productService.viewProductById(orderItem.getId());
				
				if(product!=null) {
					OrderItems orderItemss=new OrderItems();
					orderItemss.setPrice(product.getPrice());
					orderItemss.setQuantity(orderItem.getCartQuantity());
					orderItemss.setAmount(product.getPrice()*orderItem.getCartQuantity());
					
					orderItemss.setOrder(order);
					orderItemss.setProduct(product);
					orderItems.add(orderItemss);
				}
				
			}
			orderItemsService.saveAllOrderItems(orderItems);
		}else {
			order=null;
		}
		
		return order;
	}
	
	public List<OrderItems> viewOrderItemDetails(int id) {
		Order order=orderRepo.findById(id).orElse(null);
		return orderItemsRepo.findByOrder(order);
	}
	
	public List<Order> viewOrderDetails(int userid) {
		User user=userService.getUserById(userid);
		return orderRepo.findByUser(user);
	}
	
}
