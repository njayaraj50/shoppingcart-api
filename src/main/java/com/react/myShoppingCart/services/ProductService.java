package com.react.myShoppingCart.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.react.myShoppingCart.entity.Product;
import com.react.myShoppingCart.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	private ProductRepository productRepo;
	
	public List<Product> viewAllProducts(){
		return productRepo.findAll();
	}
	
	public Product viewProductById(int id){
		return productRepo.findById(id).orElse(null);
	}

}
