package com.react.myShoppingCart.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.react.myShoppingCart.BO.UserBO;
import com.react.myShoppingCart.entity.User;
import com.react.myShoppingCart.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	public User addNewUser(UserBO userBo) {
		User user=new User();
		user.setId(userBo.getId());
		user.setFirstname(userBo.getFirstname());
		user.setLastname(userBo.getLastname());
		user.setEmail(userBo.getEmail());
		user.setPassword(userBo.getPassword());
		user.setUsername(userBo.getUsername());
		user.setMobilenumber(userBo.getMobilenumber());
		userRepo.save(user);
		return user;
	}
	public User CheckLogin(String username, String password) {
		System.out.println(username);
		User user = userRepo.findbyunamepassword(username, password).orElse(null);

		/*(if (user != null) {
			int s1 = user.getUsername().compareTo(username);
			int s2 = user.getPassword().compareTo(password);
			System.out.println(s1);
			if ((s1 > 0) || (s1 < 0)) {
				user = null;
			}

			if ((s2 > 0) || (s2 < 0)) {
				user = null;
			}
		}*/
		return user;
	}

	public User getUserById(int id) {
		return userRepo.findById(id).orElse(null);
	}
}
